﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitAPI.Models
{
    public class Goal
    {
        public int GoalId { get; set; }
        public int EndDate { get; set; }
        public bool Achived { get; set; }
        public TrainingProgram TrainingProgram { get; set; }
        public int TrainingProgramId { get; set; }
        public Profile Profile { get; set; }
        public GoalWorkout GoalWorkout { get; set; }
    }
}
