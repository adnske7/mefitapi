﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MeFitAPI.Models
{
    public class ProgramWorkout
    {
       public TrainingProgram TrainingProgram { get; set; }
       public int TrainingProgramId { get; set; }
       public Workout Workout { get; set; }
       public int WorkoutId { get; set; }
    }
}
