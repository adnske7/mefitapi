﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Models
{
    public class MeFitDbContext : DbContext

    {
        public MeFitDbContext(DbContextOptions<MeFitDbContext> options) : base(options) { }

        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public DbSet<Exercise> Exercies { get; set; }
        public DbSet<GoalWorkout> GoalWorkouts { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<TrainingProgram> TrainingPrograms { get; set; }
        public DbSet<ProgramWorkout> ProgramWorkouts { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Address>().ToTable("Address");
            modelBuilder.Entity<Exercise>().ToTable("Exercise");
            modelBuilder.Entity<Goal>().ToTable("Goal");
            modelBuilder.Entity<Profile>().ToTable("Profile");
            modelBuilder.Entity<Set>().ToTable("Set");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Workout>().ToTable("Workout");
            modelBuilder.Entity<TrainingProgram>().ToTable("TrainingProgram");
            modelBuilder.Entity<ProgramWorkout>().HasKey(pq => new { pq.TrainingProgramId, pq.WorkoutId });
            modelBuilder.Entity<GoalWorkout>().HasKey(pq => new { pq.GoalId, pq.WorkoutId });
        }
    }
    
}