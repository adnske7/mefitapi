﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFitAPI.Migrations
{
    public partial class mig2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Goal_TrainingProgramId",
                table: "Goal");

            migrationBuilder.CreateIndex(
                name: "IX_Goal_TrainingProgramId",
                table: "Goal",
                column: "TrainingProgramId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Goal_TrainingProgramId",
                table: "Goal");

            migrationBuilder.CreateIndex(
                name: "IX_Goal_TrainingProgramId",
                table: "Goal",
                column: "TrainingProgramId",
                unique: true);
        }
    }
}
