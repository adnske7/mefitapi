﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFitAPI.Migrations
{
    public partial class abc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ProgramWorkouts_WorkoutId",
                table: "ProgramWorkouts");

            migrationBuilder.DropIndex(
                name: "IX_GoalWorkouts_WorkoutId",
                table: "GoalWorkouts");

            migrationBuilder.DropIndex(
                name: "IX_Goal_TrainingProgramId",
                table: "Goal");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramWorkouts_TrainingProgramId",
                table: "ProgramWorkouts",
                column: "TrainingProgramId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProgramWorkouts_WorkoutId",
                table: "ProgramWorkouts",
                column: "WorkoutId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GoalWorkouts_WorkoutId",
                table: "GoalWorkouts",
                column: "WorkoutId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Goal_TrainingProgramId",
                table: "Goal",
                column: "TrainingProgramId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_ProgramWorkouts_TrainingProgramId",
                table: "ProgramWorkouts");

            migrationBuilder.DropIndex(
                name: "IX_ProgramWorkouts_WorkoutId",
                table: "ProgramWorkouts");

            migrationBuilder.DropIndex(
                name: "IX_GoalWorkouts_WorkoutId",
                table: "GoalWorkouts");

            migrationBuilder.DropIndex(
                name: "IX_Goal_TrainingProgramId",
                table: "Goal");

            migrationBuilder.CreateIndex(
                name: "IX_ProgramWorkouts_WorkoutId",
                table: "ProgramWorkouts",
                column: "WorkoutId");

            migrationBuilder.CreateIndex(
                name: "IX_GoalWorkouts_WorkoutId",
                table: "GoalWorkouts",
                column: "WorkoutId");

            migrationBuilder.CreateIndex(
                name: "IX_Goal_TrainingProgramId",
                table: "Goal",
                column: "TrainingProgramId");
        }
    }
}
