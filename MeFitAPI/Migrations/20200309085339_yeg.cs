﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFitAPI.Migrations
{
    public partial class yeg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Profile_AddressId",
                table: "Profile");

            migrationBuilder.DropIndex(
                name: "IX_Profile_GoalId",
                table: "Profile");

            migrationBuilder.DropIndex(
                name: "IX_Profile_WorkoutId",
                table: "Profile");

            migrationBuilder.CreateIndex(
                name: "IX_Profile_AddressId",
                table: "Profile",
                column: "AddressId",
                unique: true,
                filter: "[AddressId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Profile_GoalId",
                table: "Profile",
                column: "GoalId",
                unique: true,
                filter: "[GoalId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Profile_WorkoutId",
                table: "Profile",
                column: "WorkoutId",
                unique: true,
                filter: "[WorkoutId] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Profile_AddressId",
                table: "Profile");

            migrationBuilder.DropIndex(
                name: "IX_Profile_GoalId",
                table: "Profile");

            migrationBuilder.DropIndex(
                name: "IX_Profile_WorkoutId",
                table: "Profile");

            migrationBuilder.CreateIndex(
                name: "IX_Profile_AddressId",
                table: "Profile",
                column: "AddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Profile_GoalId",
                table: "Profile",
                column: "GoalId");

            migrationBuilder.CreateIndex(
                name: "IX_Profile_WorkoutId",
                table: "Profile",
                column: "WorkoutId");
        }
    }
}
