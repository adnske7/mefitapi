﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFitAPI.Migrations
{
    public partial class def : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_GoalWorkouts_GoalId",
                table: "GoalWorkouts",
                column: "GoalId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_GoalWorkouts_GoalId",
                table: "GoalWorkouts");
        }
    }
}
